<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CardController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ListController;
use App\Http\Controllers\BoardController;
use App\Http\Controllers\OrganizationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::prefix('admin')->middleware(['auth', 'verified'])->group(function () {

    Route::get('home', [HomeController::class, 'index'])->name('home');

    Route::resource('organization', OrganizationController::class);
    Route::resource('board', BoardController::class)->except('create');
    Route::resource('form', FormController::class);
    Route::resource('list', ListController::class)->except('create');

    Route::get('/board/{id}/create', [BoardController::class, 'create'])->name('board.create');

    Route::get('/list/{id}/create', [ListController::class, 'create'])->name('list.create');

    Route::get('/card/{id}/create', [CardController::class, 'create'])->name('card.create');

    Route::post('/card',[CardController::class,'store'])->name('card.store');
});



