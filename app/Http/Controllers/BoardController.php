<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('board.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $form = Form::first();

        $queries= http_build_query([
            'name' => $request->name,
            'desc' =>$request->description,
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/boards/?" . $queries;

        $client->request('POST', $url, [
            'verify'  => false,
        ]);

        return redirect()->route('organization.show',$id)->with('success','Board created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/boards/" .$id. "/lists?" . $queries;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        // dd($url);

        $responseBody = json_decode($response->getBody());
        // dd($responseBody);

        return view('board.show', compact('responseBody','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/boards/" .$id. "?" . $queries;
        // dd($url);

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        $board = json_decode($response->getBody());

        return view('board.edit', compact('board'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $form = Form::first();

        $queries= http_build_query([
            'name' => $request->name,
            'desc' =>$request->description,
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $updateUrl = "https://api.trello.com/1/boards/" .$id. "?" . $queries;
        // dd($updateUrl);

        $client->request('PUT', $updateUrl, [
            'verify'  => false,
        ]);

        return back()->with('success','Board updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/boards/".$id."?" . $queries;

        $response = $client->request('DELETE', $url, [
            'verify'  => false,
        ]);

        return back();
    }
}
