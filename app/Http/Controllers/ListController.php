<?php

namespace App\Http\Controllers;

use App\Models\Form;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // dd($id);
        return view('list.create',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $form = Form::first();

        $queries= http_build_query([
            'name' => $request->name,
            'idBoard' => $id,
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/lists?" . $queries;
        // dd($url);

        $client->request('POST', $url, [
            'verify'  => false,
        ]);

        return redirect()->route('board.show',$id)->with('success','List created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::first();

        $queries= http_build_query([
            'oauth_consumer_key' => $form->consumer_key,
            'oauth_token' => $form->token
        ]);

        $client = new Client();
        $url = "https://api.trello.com/1/lists/" .$id. "/cards?" . $queries;

        $response = $client->request('GET', $url, [
            'verify'  => false,
        ]);

        // dd($url);

        $responseBody = json_decode($response->getBody());
        // dd($responseBody);

        return view('list.show',compact('responseBody','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
