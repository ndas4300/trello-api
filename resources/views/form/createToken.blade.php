@extends('layouts.app')

@section('content')

<div class="container">
    <form class="form" id="kt_form" method="post" action="{{ route('form.update', $form->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="token">Token</label>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control" name="token" id="token" placeholder="Token" required>
                </div>
                <div class="col-md-4">
                    <a href="https://trello.com/app-key" target="_blank">Get token</a>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
