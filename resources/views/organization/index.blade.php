@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Organizations</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <ul class="list-group">
                                @foreach ($responseBody as $organization)
                                <a href="{{route('organization.show',$organization->id)}}">
                                    <li class="list-group-item">{{$organization->name}}</li>
                                </a>
                                @endforeach
                              </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
